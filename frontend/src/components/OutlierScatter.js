import { useLayoutEffect, useRef, useState } from "react";
import ReactSelect, { components } from "react-select";
import { Slider, Typography } from "@mui/material";
import Plot from "react-plotly.js";
import { useRecoilValue } from "recoil";
import { filtersToQueryParamsState } from "../recoil/selectors";
import * as Helper from "../helper/ArrayMath";
import useResizeObserver from "@react-hook/resize-observer";

const useSize = (target) => {
  const [size, setSize] = useState();

  useLayoutEffect(() => {
    setSize(target.current?.getBoundingClientRect().width);
  }, [target]);

  useResizeObserver(target, (entry) => setSize(entry.contentRect.width));
  return size;
};

export default function OutlierScatter({ filteredEvents, rowClickCallback }) {
  const filters = useRecoilValue(filtersToQueryParamsState);
  const maxBins = 40;
  const minBinEntries = 5;
  const defaultQuantile = 98.0;
  const defaultOutlier = 99.9;
  const [quantileValue, setQuantileValue] = useState(defaultQuantile);
  const [outlierLimit, setOutlierLimit] = useState(defaultOutlier);

  const plotlyContainer = useRef(null);
  useSize(plotlyContainer);

  const handleQuantileChange = (event, value) => setQuantileValue(value);

  const handleOutlierChange = (event, value) => setOutlierLimit(value);

  const items = [
    {
      value: "area",
      label: "Area",
      disabled: filters.includes("field=") && !filters.includes("field=area"),
    },
    {
      value: "length",
      label: "Length",
      disabled: filters.includes("field=") && !filters.includes("field=length"),
    },
    {
      value: "severity_index",
      label: "Severity Index",
      disabled:
        filters.includes("field=") && !filters.includes("field=severity_index"),
    },
  ];

  const enabledItems = items.filter((obj) => {
    return !obj.disabled;
  });

  const [state, setState] = useState({
    selected: enabledItems[0].value,
  });

  const createChunks = (events, bins, entriesPerBin) => {
    if (events.length == 0) {
      return null;
    }

    if (events.length < bins * entriesPerBin) {
      if (events.length > entriesPerBin) {
        return createChunks(
          events,
          Math.floor(events.length / entriesPerBin),
          entriesPerBin
        );
      } else {
        return createChunks(events, events.length, 1);
      }
    }

    return Helper.chunks(events, bins);
  };

  const getLinearData = (data, func, func_params, probs = enabledItems) => {
    if (!data) {
      return null;
    }

    const result = {
      area: [],
      length: [],
      severity_index: [],
    };

    for (let i = 0; i < data.length; i++) {
      const date = new Date(
        data[i][Math.floor(data[i].length / 2)].start_time * 1000
      );
      probs.forEach((item) => {
        result[item.value].push({
          index: date,
          value: func(data[i], item.value, func_params),
        });
      });
    }

    return result;
  };

  const data = createChunks(filteredEvents, maxBins, minBinEntries);
  const mean = getLinearData(data, Helper.mean);

  const makePlot = () => {
    if (!data) {
      return <div>No data loaded...</div>;
    }
    const quantile = getLinearData(data, Helper.quantile, quantileValue / 100, [
      { value: state.selected },
    ]);
    const outlier = Helper.outlier(
      filteredEvents,
      state.selected,
      outlierLimit / 100
    );
    const outlier_index = [];
    outlier.forEach((entry) => {
      outlier_index.push({
        index: entry.event_id,
        date: new Date(entry.start_time * 1000),
      });
    });

    return (
      <div ref={plotlyContainer} className={"scatter-container"}>
        <Plot
          data={[
            {
              x: mean[state.selected].map((obj) => obj.index),
              y: mean[state.selected].map((obj) => obj.value),
              mode: "lines",
              type: "scatter",
              name: "mean",
            },
            {
              x: quantile[state.selected].map((obj) => obj.index),
              y: quantile[state.selected].map((obj) => obj.value),
              mode: "lines",
              type: "scatter",
              name: `${quantileValue}% quantile`,
            },
            {
              x: outlier_index.map((obj) => obj.date),
              y: outlier.map((obj) => obj[state.selected]),
              mode: "markers",
              type: "scatter",
              name: `outliers (> ${outlierLimit}%)`,
            },
          ]}
          layout={{
            autosize: true,
            margin: {
              l: 25,
              r: 30,
              t: 5,
              b: 0,
              autoexpand: true,
            },
            legend: {
              orientation: "h",
              x: -0.1,
              yanchor: "top",
              y: -0.2,
            },
            modebar: { orientation: "v" },
          }}
          config={{
            displaylogo: false,
          }}
          style={{ width: "100%", height: "100%" }}
          onClick={(data) => {
            if (data.points[0].data.mode == "markers") {
              const date = outlier_index.find(
                (obj) =>
                  obj.date.getTime() == new Date(data.points[0].x).getTime()
              );
              rowClickCallback(date.index);
            }
          }}
          onHover={(data) => {
            if (data.points[0].data.mode == "markers") {
              data.event.explicitOriginalTarget.style.cursor = "pointer";
            }
          }}
          onUnhover={(data) => {
            if (data.points[0].data.mode == "markers") {
              data.event.explicitOriginalTarget.style.cursor = "default";
            }
          }}
        />
      </div>
    );
  };

  const makeSlider = (title, min, max, defaultValue, onChange) => {
    return (
      <>
        <Typography className={"slider-description"}>{title}</Typography>
        <Slider
          size={"small"}
          min={min}
          max={max}
          defaultValue={defaultValue}
          onChangeCommitted={onChange}
          valueLabelDisplay={"auto"}
          valueLabelFormat={(value) => `${value}%`}
          step={0.1}
          marks={[
            { value: min, label: `${min}%` },
            { value: max, label: `${max}%` },
          ]}
        />
      </>
    );
  };

  return (
    <div className="plot-view-child outlier-scatter">
      <div className="outlier-scatter-config">
        <div className="text-center">
          Development of&nbsp;
          <ReactSelect
            className={"outlier-scatter-config-element outlier-scatter-select"}
            options={enabledItems}
            onChange={(selected) => {
              setState({
                selected: selected.value,
              });
            }}
            value={enabledItems.find((obj) => obj.value === state.selected)}
            isSearchable={false}
            hideSelectedOptions={true}
            components={{
              Option: (props) => {
                return (
                  <div className="text-left">
                    <components.Option {...props}>
                      {props.label}
                    </components.Option>
                  </div>
                );
              },
            }}
            styles={{
              control: (baseStyles, state) => ({
                ...baseStyles,
                padding: 0,
                width: "fit-content",
              }),
              menu: (provided, state) => ({
                ...provided,
                width: "fit-content",
                textAlign: "left",
              }),
            }}
          />
        </div>
        <div className={"outlier-scatter-config-element slider-box"}>
          <div className={"slider-box-element"}>
            {makeSlider(
              "Quantile Line",
              80.0,
              100.0,
              defaultQuantile,
              handleQuantileChange
            )}
          </div>
          <div className={"slider-box-element"}>
            {makeSlider(
              "Outlier Threshold",
              95.0,
              100.0,
              defaultOutlier,
              handleOutlierChange
            )}
          </div>
        </div>
      </div>
      {makePlot()}
    </div>
  );
}
