import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import DeleteIcon from "@mui/icons-material/Delete";

import { useRecoilState } from "recoil";
import {
  intervalAtoms,
  intervalComparisonCandidateListAtom,
} from "../recoil/atoms";
import { ToggleButtonGroup, ToggleButton } from "@mui/material";

export default function SelectedTimeIntervalListItem(props) {
  const [intervalList, setIntervalList] = useRecoilState(
    intervalComparisonCandidateListAtom
  );
  const [intervalA, setIntervalA] = useRecoilState(intervalAtoms(0));
  const [intervalB, setIntervalB] = useRecoilState(intervalAtoms(1));

  const localeOpts = {
    timeZone: "UTC",
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
  };
  const start = new Date(props.intervalRange.min)
    .toLocaleString("de-DE", localeOpts)
    .replace(" ", "\u00a0");
  const end = new Date(props.intervalRange.max)
    .toLocaleString("de-DE", localeOpts)
    .replace(" ", "\u00a0");

  const label = start + "\u00a0- " + end;

  function addToInterval(index, interval, setInterval, override = false) {
    if (!override && (interval.startDate || interval.endDate)) {
      return false;
    }

    setInterval({
      startDate: new Date(intervalList[index].min),
      endDate: new Date(intervalList[index].max),
    });

    return true;
  }

  function handleDelete(index) {
    setIntervalList((oldIntervalList) => {
      let newList = [...oldIntervalList];
      newList.splice(index, 1);
      return newList;
    });
  }

  function setActiveOnInterval(intervalName) {
    setIntervalList((oldIntervalList) => {
      let newList = [];

      for (let i = 0; i < oldIntervalList.length; i++) {
        let item = { ...oldIntervalList[i] };

        if (i == props.intervalIndex) {
          item.activeIntervalName = intervalName;
        } else if (item.activeIntervalName == intervalName) {
          item.activeIntervalName = undefined;
        }

        newList.push(item);
      }

      return newList;
    });
  }

  const intervalASelected = props.activeIntervalName === "Interval A",
    intervalBSelected = props.activeIntervalName === "Interval B";

  const unassignInterval = (intervalName, setInterval) => {
    setIntervalList(
      intervalList.map((listItem) => {
        return listItem.activeIntervalName === intervalName
          ? { ...listItem, activeIntervalName: null }
          : listItem;
      })
    );
    setInterval({});
  };

  return (
    <>
      <ListItem
        selected={props.active}
        secondaryAction={
          <IconButton
            onClick={() => handleDelete(props.intervalIndex)}
            edge="end"
            aria-label="delete"
          >
            <DeleteIcon />
          </IconButton>
        }
      >
        <ToggleButtonGroup sx={{ marginRight: 2 }}>
          <Tooltip title="Add to Interval View A">
            <ToggleButton
              value=""
              className={"text-primary"}
              selected={intervalASelected}
              onClick={() => {
                if (intervalASelected) {
                  unassignInterval(props.activeIntervalName, setIntervalA);
                } else {
                  addToInterval(
                    props.intervalIndex,
                    intervalA,
                    setIntervalA,
                    true
                  );
                  setActiveOnInterval("Interval A");
                }
              }}
              sx={{ borderWidth: "0 1px 0 0" }}
            >
              🠪<sup>A</sup>
            </ToggleButton>
          </Tooltip>
          <Tooltip title="Add to Interval View B">
            <ToggleButton
              value=""
              className={"text-primary"}
              selected={intervalBSelected}
              sx={{ borderWidth: "0" }}
              onClick={() => {
                if (intervalBSelected) {
                  unassignInterval(props.activeIntervalName, setIntervalB);
                } else {
                  addToInterval(
                    props.intervalIndex,
                    intervalB,
                    setIntervalB,
                    true
                  );
                  setActiveOnInterval("Interval B");
                }
              }}
            >
              🠪<sup>B</sup>
            </ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
        <ListItemText primary={label} secondary={props.activeIntervalName} />
      </ListItem>
    </>
  );
}
