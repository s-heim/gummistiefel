# Gummistiefel ("Rubber boots")

This tool is the result of an assignment work in the winter term 2022/23 during my Master's degree.

In the task, a set of simulated rain data was specified, which was to be visualized accordingly.

## Task Description (*German*)

*Gegeben sind Starkregenereignisse der Jahre 1979-2017 für ein Gebiet, welches Deutschland, die Schweiz und Italien umfasst. Wissenschaftlerin Caliga interessiert sich für die Veränderung der Starkregenereignisse über die Zeit. Sie möchte Veränderungen der Eigenschaften der Starkregenereignisse untersuchen und mögliche Trends identifizieren. Welche konkreten Zeitintervalle dafür zusammengefasst werden müssen, kann nicht pauschal festgelegt werden. Für definierte Zeitintervalle möchte sie einen Überblick über die Eigenschaften der Starkregenereignisse bekommen. Als globale Referenz sollen die Eigenschaften der Starkregenereignisse über den gesamten Zeitraum herangezogen werden. Für eine eingehende Untersuchung möchte Caliga zwei der definierten Zeitintervalle detailliert vergleichen. Für diesen Detailvergleich sind einzelne, besonders extreme Ereignisse relevant, bspw. das zeitlich längste, das intensivste oder räumlich ausgedehnteste Ereignis. Zur abschließenden Bewertung möchte sie die raum-zeitliche Entwicklung dieser gewählten Extremereignisse ebenfalls untersuchen und vergleichen.

Bei den gegebenen Daten handelt es sich um Simulationsdaten.*

## Implementation

The tool consists of a Python backend with Pandas and FastAPI and a ReactJS frontend as well as various libraries for visualizations. The respective dependencies can be reviewed in the subdirectories. An API generator automatically translates the backend interfaces for the frontend.

## Getting run

With a simple `docker-compose up`, you can startup the whole applications as a docker container. Otherwise you can also use the makefile.

The frontend can then be reached on port 3000 and the backend on port 8080.

## Screenshots

![](img/startup.png)

![](img/filters.png)

![](img/timeline.png)

![](img/details.png)

![](img/event-animation.png)